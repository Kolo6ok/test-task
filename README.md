how it works

```clone git 
npm install && npm run dev
docker-compose build
docker-compose up -d
docker exec -it test_l8_app composer install
docker exec -it test_l8_app php artisan migrate
```
then open -> `http://localhost:8090/`

P.S. Tasks can be dragged between columns
