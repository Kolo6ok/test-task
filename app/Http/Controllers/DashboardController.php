<?php

namespace App\Http\Controllers;

use App\Http\Requests\DashboardRequest;
use App\Http\Resources\DashboardResources;
use App\Models\Dashboard;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $userId = Auth::id();

        $assigned = DB::table('tasks')
            ->select('dashboards.id as dashboardsIds')
            ->leftJoin('columns','columns.id','=','tasks.column_id')
            ->leftJoin('dashboards','dashboards.id','=','columns.dashboard_id')
            ->where('tasks.executor_id','=',$userId)
            ->groupBy('dashboardsIds')
            ->get();

        $dashboards = Dashboard::where('user_id',$userId)
            ->orWhere('id','in',$assigned)
            ->get()
            ->map(function (Dashboard $dashboard) {
                $tasks = [];
                return [
                    'id' => $dashboard->id,
                    'name' => $dashboard->name,
                    'totalTasks' => count($tasks)
                ];
            });
        return Inertia::render('Dashboards/Index',[
            'dashboards' => $dashboards,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Inertia\Response
     */
    public function show($id)
    {
        $dashboard = $this->findModel($id);
        $users = User::all()->map(function ($user){
            return [
                'id' => $user->id,
                'name' => $user->name,
            ];
        });

        return Inertia::render('Dashboards/Show',[
            'dashboard' => new DashboardResources($dashboard),
            'users' => $users
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DashboardRequest $request)
    {
        Dashboard::create(array_merge($request->all(),['user_id' => Auth::id()]));
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DashboardRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DashboardRequest $request, $id)
    {
        $dashboard = $this->findModel($id);
        $dashboard->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dashboard = $this->findModel($id);
        $dashboard->delete();
        return back();
    }

    /**
     * @param $id
     * @return Dashboard|null
     */
    private function findModel($id)
    {
        $dashboard = Dashboard::find($id);
        abort_if(!$dashboard,404);
        return $dashboard;
    }
}
