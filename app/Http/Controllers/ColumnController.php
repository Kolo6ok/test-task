<?php

namespace App\Http\Controllers;

use App\Http\Requests\ColumnRequest;
use App\Models\Column;
use Illuminate\Http\RedirectResponse;

class ColumnController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param ColumnRequest $request
     * @return RedirectResponse
     */
    public function store(ColumnRequest $request)
    {
        Column::create($request->all());
        return back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param ColumnRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(ColumnRequest $request, $id)
    {
        $column = $this->findModel($id);
        $column->update($request->all());
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $column = $this->findModel($id);
        $column->delete();
        return back();
    }

    /**
     * @param $id
     * @return Column|null
     */
    private function findModel($id)
    {
        $column = Column::find($id);
        abort_if(!$column,404);
        return $column;
    }
}
