<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Models\Column;
use App\Models\Task;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskRequest $request
     * @return RedirectResponse
     */
    public function store(TaskRequest $request)
    {
        $task = Task::newModelInstance(array_merge($request->all(),['author_id' => Auth::id()]));
        if(empty($task->executor_id)){
            $task->executor_id = Auth::id();
        }
        $a = $task->save();
        return back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param TaskRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(TaskRequest $request, $id)
    {
        $task = $this->findModel($id);
        $task->update($request->all());
        return back();
    }


    public function dragDrop(Request $request)
    {
        $task_id= $request->post('task_id');
        $sort= $request->post('sort');
        $column_id= $request->post('column_id');
        $max = Task::where('column_id','=', $column_id)->max('sort');
        $task = Task::find($task_id);

        if ($sort == $max){
            $task->sort = $sort + 1;
        } else {
            $tasks = Task::where('column_id','=', $column_id)
                ->where('sort','>=',$sort)
                ->where('id','<>',$task_id)
                ->get();
            $task->sort = $sort;
            foreach ($tasks as $item){
                $item->sort += 1;
                $item->save();
            }
        }
        $task->column_id = $column_id;
        $task->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $task = $this->findModel($id);
        $task->delete();
        return back();
    }

    /**
     * @param $id
     * @return Task|null
     */
    private function findModel($id)
    {
        $task = Task::find($id);
        abort_if(!$task,404);
        return $task;
    }
}
