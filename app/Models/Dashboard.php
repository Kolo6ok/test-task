<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Dashboard
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard newQuery()
 * @method static \Illuminate\Database\Query\Builder|Dashboard onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard query()
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Dashboard whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|Dashboard withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Dashboard withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Column[] $columns
 * @property-read int|null $columns_count
 */
class Dashboard extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = ['name','user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function columns()
    {
        return $this->hasMany(Column::class);
    }
}
