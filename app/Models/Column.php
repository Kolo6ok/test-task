<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Column
 *
 * @property int $id
 * @property string $name
 * @property int $dashboard_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Dashboard $dashboard
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @method static \Illuminate\Database\Eloquent\Builder|Column newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Column newQuery()
 * @method static \Illuminate\Database\Query\Builder|Column onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Column query()
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereDashboardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Column withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Column withoutTrashed()
 * @mixin \Eloquent
 * @property int|null $sort
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereSort($value)
 */
class Column extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = ['name','dashboard_id','sort'];

    public function dashboard()
    {
        return $this->belongsTo(Dashboard::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
